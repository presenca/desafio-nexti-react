## Developer React.js

Parabéns, você passou para a segunda fase do processo seletivo da NEXTI para desenvolvedor (React.js).
Desta forma estamos buscando um integrante para o time trazendo destreza na parte de interface de usuário e desenvolvimento front-end

### Avaliação
- Disponibilizar o repositório público no Github e documentar o que foi utilizado e como devemos executar o projeto no README.
- Será visto no código desenvolvido: clean code, estrutura, apresentação visual, entre outros fatores, e manutenção.
- Documente todo o processo necessário para executarmos o seu projeto, para isso esperamos que utilize o README no git.
- Explique as decisões técnicas tomadas caso necessário, as escolhas por bibliotecas, ferramentas etc.

### Regras

- Montar o layout que achar adequado de acordo com o protótipo de tela passado abaixo.
- Utilizar React.js (opcional Typescript).
	- Os insumos devem ser recuperados através de uma API externa.
- As API's serão apresentadas em seguida.
- Adicionar ação onde o usuário possa escolher a linguagem do sistema: (pt-br, en-us e es-es).
- Adicionar ação onde o usuário possa escolher o tema: (Ligth ou Dark).
- Criar uma tela de login: (Usuário e senha)
	- Apenas usuário logado podem acessar o sistema.
	- Utilizar login fixo. (Ex. Login: user, Senha: 123).
	
![picture](https://desafioreact.s3.amazonaws.com/menu/layout)

### Demanda

--------------------------------------
![picture](https://desafioreact.s3.amazonaws.com/menu/component)

- Requisto componente 1:
	- Clicar em configurações deve apresentar um menu para que possa ser feito o Logout.
    
- Requisto componente 2:
	- Exibir iniciais do usuário logado.
    
- Requisto componente 3:
    - Ao clicar no botão "Arquivar" os elementos escolhidos do componente 4 devem ser excluídos da tabela.
    
- Requisto componente 4:
	- Listar a estrutura de menu a partir dos elementos adquiridos pela api: <https://desafioreact.s3.amazonaws.com/menu/menu.json>
      - Exemplo:
      	- Menu
        	- sub-menu
        	- sub-menu
      	- Menu
        	- sub-menu
    - Ao buscar um elemento (sub-menu - Urgente, por exemplo), deve atualizar a tabela simbolizada pelo componente 4, com os elementos associados ao sub-menu.
    
- Requisto componente 5:
    - Exibir as informações relativos ao elemento escolhido no componente 2, a informações estão disponíveisb em:
        - <https://desafioreact.s3.amazonaws.com/menu/itens.json>
	  	- Lembre de pesquisar pelo ID do sub-menu
    - Cada elemento do (Card) deve exibir os seguintes insumos:
      - Name (Rivaldo Ronaldo -> Primeiro texto)
      - Subject (Boa tarde, como foi o suporte realizado? -> Segundo texto)
      - Owner (RR -> Circulo maior com as iniciais)
      - Users (RR, RR, RR -> Três circulos menores com as iniciais)
      - OBS: Os demais insumos do Card podem serem fixos.
    - Quando o usuário mover o mouse sobre a linha, deve ser apresentada a opção de selecionar o elemento da tabela (Mostrar um checkbox no lugar das iniciais do Owner).
    - Ao escolher o elemento, todas as Iniciais devem ser mostradas como opção de seleção para autorizar multiplas escolhas.
    - Ao cancelar todas as opções, o sistema deve voltar a mostrar as Iniciais.
   
--------------------------------------

### Atenção
- O desenvolvimento do frontend deve ser feito em React.js.
- Boa documentação de código e de serviços.
- Desenvolver uma estrutura em front-end utilizando as melhores práticas.
- Não é necessário submeter uma aplicação que cumpra cada um dos requisitos descritos, mas o que for submetido deve funcionar.
- Boas práticas de UX na solução.
- Testes do código.
- Caso o processo de execução não esteja documentado no README o teste será automaticamente reprovado.



### Finalizando
- Qualquer dúvida, estamos à disposição.